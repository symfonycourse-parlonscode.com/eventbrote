<?php

namespace App\Controller;

use App\Repository\EventRepository;
use App\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EventsController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @Route("/events", name="events.index")
     */
    public function index(EventRepository $repo): Response
    {
        $events = $repo->findAll();
        return $this->render('events/index.html.twig', compact('events'));
    }

    /**
     * @Route("/events/{id<[0-9]*>}", name="events.show")
     */
    public function show(Event $event): Response
    {
        return $this->render('events/show.html.twig', compact('event'));
    }
}