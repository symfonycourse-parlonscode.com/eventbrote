<?php

namespace App\Service;

use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\CacheInterface;


class MarkdownTranformer
{
    private $parser;
    private $cache;

    public function __construct(MarkdownParserInterface $markdownParser, CacheInterface $cache)
    {
        $this->parser = $markdownParser;
        $this->cache = $cache;
    }
    public function parse(string $str): string
    {
        $key = 'key_' . md5($str);
        $html = $this->cache->get($key, function(ItemInterface $item) use ($str) {
            return $this->parser->transformMarkdown($str);
        });
        return $html;
    }
}
