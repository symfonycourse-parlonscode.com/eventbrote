<?php

namespace App\Twig;

use App\Entity\Event;
use App\Service\MarkdownTranformer;
use InvalidArgumentException;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $parser;

    public function __construct(MarkdownTranformer $markdownTranformer)
    {
        $this->parser = $markdownTranformer;
    }
   
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('markdownify', [$this, 'parseMarkdown'], ['is_safe' => ['html']]),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pluralize', [$this, 'pluralize']),
            new TwigFunction('format_price', [$this, 'formatPrice']),
        ];
    }
    
    public function parseMarkdown(string $value): string
    {
        return $this->parser->parse($value);
    }

    public function pluralize(int $count, string $singular, String $plural = null): string
    {
        if (!is_numeric($count)) {
            throw new InvalidArgumentException('$count must be a numeric value');
        }

        $plural = $plural ?? $singular . 's';

        switch ($count) {
            case 1:
                $string = $singular;
                break;
            
            default:
                $string = $plural;
                break;
        }
        return sprintf("%d %s", $count, $string);
    }

    public function formatPrice(Event $event): string
    {
        if ($event->isFree()) {
            return 'FREE !';
        }
        
        $fmt = new \NumberFormatter('fr_FR', \NumberFormatter::CURRENCY);
        return numfmt_format_currency($fmt, $event->getPrice(), 'EUR');
    }
}
